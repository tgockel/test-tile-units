#!/usr/bin/env python2
from random import randint

class Map():
    def __init__(self, rows, cols):
        self._rows = rows
        self._cols = cols
        self._data = [[None] * cols for r in xrange(rows)]
    
    def clone(self):
        out = Map(self._rows, self._cols)
        for pos, item in self.allTiles():
            out[pos] = item
        return out
    
    def __getitem__(self, pos):
        r, c = pos
        return self._data[r][c]
    
    def __setitem__(self, pos, val):
        r, c = pos
        self._data[r][c] = val
    
    def on(self, pos):
        r, c = pos
        return r < 0 or c < 0 or r > self._rows or c > self._cols
    
    def allTiles(self):
        for ridx, row in enumerate(self._data):
            for cidx, item in enumerate(row):
                yield ((ridx, cidx), item)
    
    def __str__(self):
        hframe = '*' + '-'*len(self._data[0]) + '*'
        return hframe + '\n' \
             + '\n'.join('|' + ''.join(' ' if x is None else ' ' if not str(x) else str(x)[0] for x in row) + '|'
                          for row in self._data
                         ) \
             + '\n' + hframe

def distSq(a, b):
    ar, ac = a
    br, bc = b
    return float(ar - br)**2 + float(ac - bc)**2

def dist(a, b):
    return distSq(a, b) ** 0.5

def groupUnits(m, at):
    origUnits = [(pos, val) for pos, val in m.allTiles() if val is not None]
    origUnits.sort(key=lambda x: distSq(x[0], at))
    
    for origPos, valToMove in origUnits:
        goals = [pos for pos, val in m.allTiles() if val is None or val==valToMove]
        # Sort by max combined distance: How far does the unit have to move and how far is that spot from the goal?
        # The choice of exponential factor here is somewhat arbitrary, but it represents a bias towards being close to
        # the center vs moving far.
        goals.sort(key=lambda x: dist(x, at)**2 + dist(origPos, x))
        
        dest = goals[0]
        m[dest], m[origPos] = m[origPos], m[dest]
    return m

# 7 man line, centered at (5, 5)
m = Map(11, 11)
for r, x in enumerate('ABCDEFG'):
    m[r+2, 5] = x

print 'ORIGINAL'
print m

print 'GROUP AT (5, 5)'
print groupUnits(m.clone(), (5, 5))

print 'GROUP AT (9, 3)'
print groupUnits(m.clone(), (9, 3))

m = Map(20, 80)
for x in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
    while True:
        pos = (randint(0, m._rows-1), randint(0, m._cols-1))
        if m[pos] is None:
            m[pos] = x
            break
print 'ORIGINAL'
print m

for x in xrange(5):
    pos = (randint(0, m._rows-1), randint(0, m._cols-1))
    print 'GROUP AT', pos
    print groupUnits(m.clone(), pos)


m = Map(8, 8)
for idx, x in enumerate('ABCDEFGHIJKLMNOP'):
    m[(idx//4)*2, (idx%4)*2] = x
print 'ORIGINAL'
print m

print 'GROUP AT (3, 3)'
print groupUnits(m.clone(), (3, 3))
